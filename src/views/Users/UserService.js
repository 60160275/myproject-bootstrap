const userService = {
  userList: [
    {
      id: 1,
      name: 'Worawit',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Prasob',
      gender: 'M'
    }
  ],
  lastID: 3,
  addUser (user) {
    user.id = this.lastID++
    this.userList.push(user)
  },
  updataeUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
